function [T,N,shield,rad,dep,userlabel,binwidth] = importfunc(iso,sample,class,path,format)
%class is meteoriteclass = CC,OC
%iso= isotope (f.ex. 21 for 21Ne etc)
%Sample= name of sample, e.g. Flensburg
%path=folder in which the samples lie, eg L:\cornelia-mertens\CM01\

importpath=sprintf('%s%s_NobleGases_%s.%s',path,class,sample,format);
if strcmp(class,'CC')
    
    switch iso
        case 38
            opts=detectImportOptions(importpath,'Sheet',10,'VariableNamesRange','A3');
            shieldopts=detectImportOptions(importpath,'Sheet',7,'VariableNamesRange','A3','DataRange','A4');
            userlabel='P_{38}_{Ar} [10^{-8}cm^3 STP/g/Ma]';
            binwidth=0.002;
            xidx=12; %specify in which colums xses, depending on chemistry are given 
        case 3
            opts=detectImportOptions(importpath,'Sheet',2,'VariableNamesRange','A3');
            shieldopts=detectImportOptions(importpath,'Sheet',7,'VariableNamesRange','A3','DataRange','A4');
            userlabel='P_{3}_{He} [10^{-8}cm^3 STP/g/Ma]';
            binwidth=0.08; %defining bin width for the probability histograms
            xidx=15;
        case 21
            opts=detectImportOptions(importpath,'Sheet',5,'VariableNamesRange','A3');
            shieldopts=detectImportOptions(importpath,'Sheet',7,'VariableNamesRange','A3','DataRange','A4');
            userlabel='P_{21}_{Ne} [10^{-8}cm^3 STP/g/Ma]';
            binwidth=0.01;
            xidx=17;
        case 2221
            opts=detectImportOptions(importpath,'Sheet',7,'VariableNamesRange','A3','DataRange','A4');
            userlabel='^{22}Ne/^{21}Ne';
            binwidth=0.02;
            xidx=12;
        otherwise
            error('Iso %d not supported!',iso);
    end
    
elseif strcmp(class,'OC')
    switch iso
        case 38
            opts=detectImportOptions(importpath,'Sheet',10,'VariableNamesRange','A3');
            userlabel='P_{38}_{Ar} [10^{-8}cm^3 STP/g/Ma]';
            binwidth=0.002;
            xidx=14;
        case 3
            opts=detectImportOptions(importpath,'Sheet',2,'VariableNamesRange','A3');
            userlabel='P_{3}_{He} [10^{-8}cm^3 STP/g/Ma]';
            binwidth=0.08; %defining bin width for the probability histograms
            xidx=17;
        case 2221
            opts=detectImportOptions(importpath,'Sheet',7,'VariableNamesRange','A3','DataRange','A4');
            userlabel='^{22}Ne/^{21}Ne';
            binwidth=0.02;
            xidx=14;
        case 21
            opts=detectImportOptions(importpath,'Sheet',5,'VariableNamesRange','A3');
            shieldopts=detectImportOptions(importpath,'Sheet',7,'VariableNamesRange','A3','DataRange','A4');
            userlabel='P_{21}_{Ne} [10^{-8}cm^3 STP/g/Ma]';
            binwidth=0.01;
            xidx=19;
    end

end
    T=readtable(importpath,opts);
    shieldT=readtable(importpath,shieldopts);
    lim=795;
    N=T.YourChemistry(1:lim);
    rad=T.Radius_cm_(1:lim);
    dep=T.to(1:lim);%depth
    shield=shieldT.YourChemistry(1:lim);
    
