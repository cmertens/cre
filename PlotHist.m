function PlotHist(xVal,HistVal,WeighedP,userlabel,confi95,confi95plus,ProbableP)

%to plot a contnous distribution, we create a dataset to which we can fit a
%lognormal/chisquared/whatever other desired skewed pdf out of the
%calculated weighed histogram. 

val=HistVal/nansum(WeighedP)*100;
idx95=find(xVal==confi95);
idx95plus=find(xVal==confi95plus);
b1=bar(xVal,val,'FaceColor',[ 0.1    0.6  1]);
hold on
b2=bar(xVal(idx95:idx95plus),val(idx95:idx95plus),'FaceColor',[  0.2740    0.2280    0.8612]);

xlabel(userlabel)
ylabel('Sum probability over all r [%]')
% xline(confi99,'Color',[0 0.5 0.3]);
% xline(confi99plus,'Color',[0 0.5 0.3]);
l=xline(confi95,'Color',[0 0.3 0.5,0.3],'LineStyle',':','LineWidth',1.);
l2=xline(confi95plus,'Color',[0 0.3 0.5,0.3],'LineStyle',':','LineWidth',1.);
p=xline(ProbableP,'Color',[0.85 0.325 0.098],'LineWidth',1.5); %probable production rate
legend([l,p],'95% CI','Most Probable','Location','northwest')

ylim([0 max(val)*1.1])
end

