% This script calculates Probable Production rates and lets you exclude radius & depth values.
% Copyright Cornelia Mertens 2020
%-------------------------------------------------------------------------------------------------------
%% Important steps before running the script: 

%1) Specify Import paths and inputs  below in "import data" and "specify inputs"
%2) Make sure the "current folder" on the left is active. For this, go up one
%   level> right click folder > add to path > Selected folders
%3) Input your 21/22Ne and chemistry in Leyas file and save. This script
%   acceses the Leya file to load production rates (in importfunc) and exclude
%   production rates due to shielding. If headings etc are changed you will receive
%   an error message.
%--------------------------------------------------------------------------------------------------------
%%
close all; clear variables; clc;
%% import data
iso=21; %Define which isotope your looking at (21=ne21, 2221=ne22/21, 3=He3, 38=Ar38)
class='CC';% is the meteoriteclass = CC or OC?
sample='COs';% name of sample, e.g. Flensburg
format='xlsx'; %check the file format of the Leya Excel file
%your Leya file should be named e.g. CC_NobleGases_COs.xlsx
path='C:\Users\Cornelia\Documents\MATLAB\ProbabiltyOfProductionRates\';%folder in which the Leya Excel sheet is, eg L:\cornelia-mertens\CM01\

[T,N,shield,rad,dep,userlabel,binwidth] = importfunc(iso,sample,class,path,format);

%% specify inputs
percent=0.915; %percent of mass lost in the atmosphere 
innerpercent=0.3721; %percent of the radius from the center that you want to be discarded 
                     %(0.2175=21.75% of inner radius = 1% of the volume
                     % 0.3721=37.21% of inner radius = 5% of the volume)
rho=2.9; %density of the sample

shielding=1.15; %22/21Ne. You can use this shielding indicator to restrict production rates.If you 
                %cannot rstrict production rates, use an shielding error of
                %1. This will enable all production rates.
                
shieldingerr=1; %22/21Ne shielding error
indi=1;%show individual plots? If yes indi=1, if no, indi=0;
exclude=[]; %specify if you want to exclude any radii
binwidth=0.008; %The binwidth for the histograms is specified in importfunc, but if you are
                %not happy with it you can change it here
%% specify colour map
allr_unchanged=[10 20 25 30 35 40 50 65 85 100 120 150 200 300 500];
colorspec=lines(7);
colorspec(8:14,:)=abs(lines(7)-0.2);
colorspec(15,:)=copper(1);   
%% Interpolating
step=100; %how many values do you want to interpolate from ingo Leyas Production rates?
InterpolatedP=interpolating(dep,N,step); %dep=depthsteps, N=Productionrates,bzw Ne22/21,step= no. of steps used for interpolating

%% Spheric Shell probabilities

stepno=linspace(0,1,step);
TF=find(ischange(dep,'linear')); %find abrupt changes in the radius dataset
TF=[1;TF;numel(dep)+1];          %add the first and the last number
P=zeros(step,0);
allr=zeros(numel(TF)-1,0);
Rstep=zeros(step,numel(TF)-1);
for i=1:numel(TF)-1
    r=dep(TF(i+1)-1); 
    d=linspace(0,r,step+1);
    for j=1:step
    P(j)=((r-d(j))^3-(r-d(j+1))^3)/(r^3);
    end
    allr(i)=r;
    Rstep(:,i)=r*stepno;
end
Probability=repmat(P',1,15)*100;

%% Assuming no mass loss in Atmosphere
%--------------------------------------------------------------------------

%binwidth=0.0002; % you can change the binwidth here if desired
%Probability Distribution
WeighedP=InterpolatedP(:).*Probability(:);
ProductionRates=InterpolatedP(:);
[xVal,HistVal]=weightedHistogram(WeighedP,ProductionRates,binwidth);
% find 95% and 99% confidence interval
[confi95,confi95plus,ProbableP] = histCIs(xVal,HistVal,WeighedP);

%Plot Histogram
if indi==1
figure('Name','Assuming no mass has been lost')      
PlotHist(xVal,HistVal,WeighedP,userlabel,confi95,confi95plus,ProbableP)
%Plot Production rates
figure('Name','Assuming no mass has been lost')   
PlotProduction(Rstep,allr,InterpolatedP,colorspec,allr_unchanged,userlabel,confi95,confi95plus,ProbableP,1)
end

%% Excluding mass lost in the atmosphere
%--------------------------------------------------------------------------

tit=sprintf('Assuming %.1f %% has been lost in atmosphere',(percent)*100);
%calculating amount lost in atmosphere

V_loss=percent/rho;
r_loss=nthroot((3*V_loss)/(4*pi),3);
r_max=allr-r_loss;

%Cutting off Production rates and Probabilities according to mass loss

[~,idx]=min(abs(Rstep(:,1)-r_max(1)));
idx=step-idx;
Rstep2=Rstep(idx:end,:);
Pstep2=Probability(idx:end,:);
allint2=InterpolatedP(idx:end,:);
data2=allint2(:).*Pstep2(:);
v2=allint2(:);
binwidth2=0.002;
[xval2,bins2]=weightedHistogram(data2,v2,binwidth);

% find 95% and 99% confidence interval
[confi95loss,confi95plusloss,ProbablePloss] = histCIs(xval2,bins2,data2);

%Plot Histogram
if indi==1
figure('Name',tit)      
PlotHist(xval2,bins2,data2,userlabel,confi95loss,confi95plusloss,ProbablePloss)
%Plot Production rates
figure('Name',tit)  
PlotProduction(Rstep2,allr,allint2,colorspec,allr_unchanged,userlabel,confi95loss,confi95plusloss,ProbablePloss,1)
end


%% Excluding inner percentage of the Radius
%--------------------------------------------------------------------------

%binwidth=0.002;
tit2=sprintf(' + Excluding inner %0.2f %% of radius',innerpercent*100);

%cutting off production rates and probabilities
rcut=innerpercent*allr;
rcut=allr-rcut;
[val,idx2]=min(abs(Rstep(:,1)-rcut(1)));
Rstep3=Rstep(idx:idx2,:);
Pstep3=Probability(idx:idx2,:);

%Probability Distribution
allint3=InterpolatedP(idx:idx2,:);
data=allint3(:).*Pstep3(:); %data is a vector containing the interpolated Production rates
                            %times their respective probability
v=allint3(:);               %v contains just the interpolated  Production rates
[xval,bins]=weightedHistogram(data,v,binwidth);

% find 95% and 99% confidence interval
[confi95rad,confi95plusrad,ProbablePrad] = histCIs(xval,bins,data);

%Plot Histogram
if indi==1
figure('Name',tit2)   
PlotHist(xval,bins,data,userlabel,confi95rad,confi95plusrad,ProbablePrad)
%Plot Production rates
figure('Name',tit2)  
PlotProduction(Rstep3,allr,allint3,colorspec,allr_unchanged,userlabel,confi95rad,confi95plusrad,ProbablePrad,1)
end


%% Considering 22/21 Shielding and custom radii
%---------------------------------------------------------------------------------

tit3=sprintf('Allowed based on ^{22}Ne/^{21}Ne');
notx=(shielding-shieldingerr)<shield&shield<(shielding+shieldingerr); %find all entries that are allowed based on 22/21Ne shielding
depnot=dep(notx);%extract only entries from depth 
radnot=rad(notx);
Nnot=N;
Nnot(~notx)=NaN;
%interpolate
[intnot,nearestintnot]=interpolating(dep,Nnot,step); %interpolating only with allowed production rates
in=~isnan(nearestintnot)&isnan(intnot);
intnot(in)=nearestintnot(in);

%exclude any specific Radii defined in exclude above
 ex=find(ismember(allr,exclude));
 intnot(:,ex)=NaN;
 ex2=find(ismember(radnot,exclude));
 radnot(ex2,:)=[];

%Probability Distribution
allint4=intnot;
allint4=allint4(idx:idx2,:);
allrnot=allr_unchanged(any(allint4));

data4=allint4(:).*Pstep3(:); %Production rates times probability
v4=allint4(:);
binwidth4=0.002;

[xval4,bins4]=weightedHistogram(data4,v4,binwidth);


% find 95% and 99% confidence interval
[confi95x,confi95plusx,ProbablePx] = histCIs(xval4,bins4,data4);

%Plot Histogram
if indi==1
figure('Name',tit3)    
PlotHist(xval4,bins4,data4,userlabel,confi95x,confi95plusx,ProbablePx)
%Plot Production rates
figure('Name',tit3)
PlotProduction(Rstep3,allr,allint4,colorspec,allr_unchanged,userlabel,confi95x,confi95plusx,ProbablePx,1)
end


%% Overview plots

% Histograms
%--------------------------------------------------------------------------------
figure('Name','Probability distribution over all r','Position', [50 200 900 600])
subplot(2,2,1)
PlotHist(xVal,HistVal,WeighedP,userlabel,confi95,confi95plus,ProbableP)
title('Assuming no mass loss');

subplot(2,2,2)
PlotHist(xval2,bins2,data2,userlabel,confi95loss,confi95plusloss,ProbablePloss)
title(tit);

subplot(2,2,3)
PlotHist(xval,bins,data,userlabel,confi95rad,confi95plusrad,ProbablePrad)
title(tit2);

subplot(2,2,4)
PlotHist(xval4,bins4,data4,userlabel,confi95x,confi95plusx,ProbablePx)
title(tit3);

% Production rates vs Depth
%------------------------------------------------------------------------------
figure('Name','Production rates vs depth','Position', [400 200 900 400])
plotHandle=subplot(2,2,1);
set(plotHandle,'Position',[0.07 0.6 0.3 0.35]);

PlotProduction(Rstep,allr,InterpolatedP,colorspec,allr_unchanged,userlabel,confi95,confi95plus,ProbableP,0)
legHandle = get(plotHandle,'Children');
legend(flip(legHandle),'Units','normalized','Location',[0.85 0.1, 0.1, 0.8]);
title('Assuming no mass loss')

plotHandle=subplot(2,2,2);
set(plotHandle,'Position',[0.43 0.6 0.3 0.35]);
PlotProduction(Rstep2,allr,allint2,colorspec,allr_unchanged,userlabel,confi95loss,confi95plusloss,ProbablePloss,0)
title(tit);

plotHandle=subplot(2,2,3);
set(plotHandle,'Position',[0.07 0.1 0.3 0.35]);
PlotProduction(Rstep3,allr,allint3,colorspec,allr_unchanged,userlabel,confi95rad,confi95plusrad,ProbablePrad,0)
title(tit2);

plotHandle=subplot(2,2,4);
set(plotHandle,'Position',[0.43 0.1 0.3 0.35]);
PlotProduction(Rstep3,allr,allint4,colorspec,allr_unchanged,userlabel,confi95x,confi95plusx,ProbablePx,0)
title(tit3);

%% Output

f=repmat('%0.0f ',1,size(allrnot,2)); 
f=['Radius: ' f '\n'];
fprintf('\n%s\n',repmat('=',1,50));
fprintf('Probable Production Rate: %0.4f \n',ProbablePx) 
fprintf('Max Production Rate at 95%% CI: %0.4f \n',confi95plusx) 
fprintf('Min Production Rate at 95%% CI: %0.4f \n',confi95x) 
fprintf(f,allrnot)
fprintf('%s\n',repmat('=',1,50));


